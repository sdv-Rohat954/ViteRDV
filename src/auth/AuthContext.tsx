import React, {useState} from "react";

export const AppContextValue = () => {
    const [isLogged, setIsLogged] = useState(false);
    const login = () => setIsLogged(true)
    const logout = () => setIsLogged(false)
    return { isLogged, login, logout }
}
