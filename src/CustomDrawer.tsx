import React, {useContext, useEffect} from 'react';
import {NavigationContainer} from "@react-navigation/native";
import {createNativeStackNavigator} from "@react-navigation/native-stack";
import SignInScreen from "./screens/SignInScreen";
import SignUpScreen from "./screens/SignUpScreen";
import HomeScreen from "./screens/HomeScreen";
import {AuthContext} from "../App";

const Stack = createNativeStackNavigator()

export default function CustomDrawer() {
    const { isLogged } = useContext(AuthContext)

    useEffect(() => {
    }, [isLogged])

    return (
        <NavigationContainer>
                {!isLogged ? (
                    <Stack.Navigator
                        screenOptions={{
                            headerShown: false
                        }}
                    >
                        <Stack.Screen name="SignIn" component={SignInScreen}/>
                        <Stack.Screen name="SignUp" component={SignUpScreen}/>
                    </Stack.Navigator>
                ) : (
                    <HomeScreen/>
                )}
        </NavigationContainer>
    );
};