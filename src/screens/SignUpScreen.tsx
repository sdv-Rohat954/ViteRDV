import React from 'react';
import {View, StyleSheet, Pressable, Text} from 'react-native';
import SignUpForm from '../components/SignUpForm';

const SignUpScreen = ({ navigation }: { navigation: any }) => {
  return (
    <View style={styles.container}>
      <SignUpForm />
      <Pressable
          onPress={() => navigation.navigate('SignIn')}
          style={{ marginBottom: 40 }}
      >
        <Text style={{ color: '#4FCFC0' }}>Se connecter</Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default SignUpScreen;