import React from 'react';
import {View, StyleSheet, Text, Pressable} from 'react-native';
import SignInForm from '../components/SignInForm';

const SignInScreen = ({ navigation }: {navigation: any}) => {
  return (
    <View style={styles.container}>
      <SignInForm />
      <Pressable
          onPress={() => navigation.navigate('SignUp')}
          style={{ marginBottom: 40 }}
      >
      <Text style={{ color: '#4FCFC0' }}>Pas encore de compte ? S'incrire ici</Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default SignInScreen;