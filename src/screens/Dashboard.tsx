import React, {useState} from 'react';
import {FlatList, StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native';
import CustomPressable from "../components/CustomPressable";

// Demo for FlatList
const doctors = [
    { id: 1, name: 'Dr. Jean Dupont', date: '2023-06-10', status: 'À venir' },
    { id: 2, name: 'Dr. Marie Lefevre', date: '2023-06-05', status: 'Passé' },
    { id: 3, name: 'Dr. Marc Dubois', date: '2023-06-12', status: 'À venir' },
];

const Dashboard = ({ navigation }: {navigation: any}) => {
    const [searchText, setSearchText] = useState('');
    const [filter, setFilter] = useState('all');

    const navigateToProfile = (doctorId: number) => {
        navigation.navigate('Profile', { id: doctorId }); // la navigation c'est comment
    }

    const filteredDoctors = doctors.filter((doctor) => {
        if (filter === 'upcoming') {
            return doctor.status === 'À venir';
        } else if (filter === 'past') {
            return doctor.status === 'Passé';
        } else {
            return true;
        }
    }).filter((doctor) => {
        return doctor.name.toLowerCase().includes(searchText.toLowerCase());
    });

    const renderItem = ({ item }: { item: Doctor }) => (
        <TouchableOpacity style={styles.item}>
            <Text style={styles.doctorDate}>{item.date}</Text>
            <Text onPress={() => navigateToProfile(item.id)} style={styles.doctorName}>{item.name}</Text>
            <Text style={styles.doctorStatus}>{item.status}</Text>
        </TouchableOpacity>
    );

    return (
        <View style={styles.container}>
            <TextInput
                style={styles.searchInput}
                placeholder="Recherche"
                value={searchText}
                onChangeText={setSearchText}
            />

            <CustomPressable onPress={() => {}} title="Prendre un rendez-vous" />

            <View style={styles.filtersContainer}>
                <TouchableOpacity
                    style={styles.filterButton}
                    onPress={() => setFilter('all')}
                >
                    <Text style={[styles.filterButtonText, filter === 'all' && styles.activeFilter]}>Tous</Text>
                </TouchableOpacity>


                <TouchableOpacity
                    style={styles.filterButton}
                    onPress={() => setFilter('upcoming')}
                >
                    <Text style={[styles.filterButtonText, filter === 'upcoming' && styles.activeFilter]}>À venir</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={styles.filterButton}
                    onPress={() => setFilter('past')}
                >
                    <Text style={[styles.filterButtonText, filter === 'past' && styles.activeFilter]}>Passés</Text>
                </TouchableOpacity>
            </View>

            <FlatList
                data={filteredDoctors}
                renderItem={renderItem}
                keyExtractor={(item) => item.id.toString()}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 16,
    },
    searchInput: {
        height: 40,
        backgroundColor: 'white',
        paddingHorizontal: 8,
        marginBottom: 16,
        borderRadius: 4,
        color: '#A9A9A9',
    },
    filtersContainer: {
        flexDirection: 'row',
        marginBottom: 16,
    },
    filterButton: {
        paddingHorizontal: 12,
        paddingVertical: 6,
        marginRight: 8,
        backgroundColor: '#eee',
        borderRadius: 4,
    },
    filterButtonEnd: {
        textAlign: 'right',
    },
    activeFilter: {
        color: '#4FCFC0',
    },
    filterButtonText: {
        fontWeight: 'bold',
    },
    item: {
        marginBottom: 8,
        padding: 12,
        backgroundColor: '#fafafa',
        borderRadius: 4,
    },
    doctorName: {
        fontWeight: 'bold',
        fontSize: 16,
        marginBottom: 4,
    },
    doctorDate: {
        color: 'gray',
        marginBottom: 4,
    },
    doctorStatus: {
        fontWeight: 'bold',
    },
    button: {
        padding: 12,
        marginBottom: 12,
        backgroundColor: '#4FCFC0',
        borderRadius: 4,
    },
    buttonTxt: {
        color: 'white',
        alignSelf: 'center',
        fontWeight: 'bold',
    }
});

export default Dashboard;

interface Doctor {
    id: number;
    name: string;
    date: string;
    status: string;
}
