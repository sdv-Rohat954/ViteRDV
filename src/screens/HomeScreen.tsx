import React, {useContext} from 'react';
import Dashboard from "./Dashboard";
import ProfileScreen from "./ProfileScreen";
import {createDrawerNavigator, DrawerContentScrollView, DrawerItemList} from "@react-navigation/drawer";
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {AuthContext} from "../../App";

const Drawer = createDrawerNavigator()

// @ts-ignore
export const CustomDrawerContent = (props) => {
    const { logout } = useContext(AuthContext)

    return (
        <View style={{flex: 1, position: 'relative'}}>
            <DrawerContentScrollView scrollToOverflowEnabled={false}>
                <View style={styles.profileContainer}>
                    <Image
                        style={styles.profileImage}
                        source={{uri: 'https://randomuser.me/api/portraits/men/82.jpg'}}
                    />
                    <Text style={{
                        paddingBottom: 20,
                        paddingLeft: 10,
                        color: '#4FCFC0',
                        fontWeight: 'bold'
                    }}>
                        Vladimir Wonja
                    </Text>
                </View>
                <DrawerItemList {...props} />
            </DrawerContentScrollView>
            <TouchableOpacity
                onPress={() => logout()}
                style={{
                flex: 1,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                position: 'absolute',
                bottom: 20,
                marginLeft: 20,
                backgroundColor: 'rgba(255,65,65,0.45)',
                padding: 10,
                borderRadius: 10
            }}>
                <Text style={{fontWeight: 'bold', color: 'red'}}>Déconnexion</Text>
            </TouchableOpacity>
        </View>
    )
}

export default function HomeScreen() {
    return (
        <Drawer.Navigator
            initialRouteName="Dashboard"
            drawerContent={(props) => <CustomDrawerContent {...props} />}
            screenOptions={{
                drawerType: 'front',
                swipeEdgeWidth: 65,
                swipeMinDistance: 30,
                drawerActiveTintColor: '#4FCFC0',
                drawerContentStyle: {
                    backgroundColor: 'blue'
                }
            }}
        >
            <Drawer.Screen name="Dashboard" component={Dashboard}/>
            <Drawer.Screen name="Profile" component={ProfileScreen}/>
        </Drawer.Navigator>
    );
};

const styles = StyleSheet.create({
    profileImage: {
        width: 50,
        height: 50,
        borderRadius: 50
    },
    profileContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end',
        minHeight: 240,
        padding: 20,
        marginBottom: 20,
        marginTop: -50,
        backgroundColor: '#ECF1FA',
        shadowOffset: {width: 0, height: -20},
        shadowRadius: 20,
        shadowColor: 'black'
    }
})