import React from 'react';
import { TouchableOpacity, Text, StyleSheet, TouchableOpacityProps } from 'react-native';

interface ButtonProps extends TouchableOpacityProps {
  title: string;
  status?: 'default' | 'error' | 'warning' ;
}

const Button: React.FC<ButtonProps> = ({ onPress, title, style, status='default'}) => {

  
  const buttonStyles = [styles.button, style];
  const buttonTextStyles = [styles.buttonText];

  if (status === 'error') {
    buttonStyles.push(styles.errorButton);
    buttonTextStyles.push(styles.errorButtonText);
  }
  if (status === 'warning') {
    buttonStyles.push(styles.warningButton);
    buttonTextStyles.push(styles.warningButtonText);
  }

  return (
    <TouchableOpacity style={buttonStyles} onPress={onPress} >
      <Text style={buttonTextStyles}>{title}</Text>
    </TouchableOpacity>
  );
};



const styles = StyleSheet.create({
  button: {
    backgroundColor: '#4FCFC0',
    paddingVertical: 12,
    paddingHorizontal: 24,
    borderRadius: 4,
    alignItems: 'center',
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
   errorButton: {
    backgroundColor: 'red',
  },
  errorButtonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
  warningButton: {
    backgroundColor: 'orange',
  },
  warningButtonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
  });
  

export default Button;