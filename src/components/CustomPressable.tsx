import React from 'react';
import { Pressable, Text, StyleSheet } from 'react-native';

interface CustomPressableProps {
    onPress: () => void;
    title: string;
}

const CustomPressable: React.FC<CustomPressableProps> = ({ onPress, title }) => {
    return (
        <Pressable style={styles.button} onPress={onPress}>
            <Text style={styles.buttonTxt}>{title}</Text>
        </Pressable>
    );
};

const styles = StyleSheet.create({
    button: {
        padding: 12,
        marginBottom: 12,
        backgroundColor: '#4FCFC0',
        borderRadius: 4,
    },
    buttonTxt: {
        color: 'white',
        alignSelf: 'center',
        fontWeight: 'bold',
    },
});

export default CustomPressable;
