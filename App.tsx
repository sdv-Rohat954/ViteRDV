import 'react-native-gesture-handler';
import * as React from "react";
import {createContext, useState} from "react";
import CustomDrawer from "./src/CustomDrawer";

export const AuthContext = createContext({isLogged: false, login() {}, logout() {}})

export default function App() {
    const [isLogged, setIsLogged] = useState(false);
    const login = () => {
        setIsLogged(true)
    }
    const logout = () => {
        setIsLogged(false)
    }

    return (
        <AuthContext.Provider value={{ isLogged, login, logout }}>
            <CustomDrawer/>
        </AuthContext.Provider>
    );
}
